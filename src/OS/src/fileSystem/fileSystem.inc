!! -------------------------------------------------------------------------- !!
Subroutine CreateDir( dirPath, isError, isExist, isCreate )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: dirPath
    Logical, Optional            :: isError
    Logical, Optional            :: isExist
    Logical, Optional            :: isCreate
!! -------------------------------------------------------------------------- !!
    Logical :: isError_, isExist_, isCreate_    
    Character(len=len(dirPath)) :: convPath

    isError_  = .FALSE.
    isExist_  = .FALSE.
    isCreate_ = .FALSE.

    !!... Check the given 'dirPath' has a length
    if ( len_trim(dirPath).GE.1 ) Then

        Call ConvertPathOS( dirPath, convPath )

        !!... Check the directory exists
        isExist_ = IsExistDir( convPath )

        !!... Create directory if not exist
        If ( .NOT.isExist_ ) then
#ifdef __OS_LINUX__
            Call System( "mkdir -p "//trim(convPath) )
#else
            Call System( "mkdir "//trim(convPath) )
#endif
            !!... Check the directory is created
            isCreate_ = IsExistDir( convPath )
        End if
    else
        isError_ = .TRUE.
    end if

    If ( Present(isError) ) isError = isError_
    If ( Present(isExist) ) isExist = isExist_
    If ( Present(isCreate) ) isCreate = isCreate_

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
    Subroutine DeleteDir( dirPath, isDelete )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: dirPath
    Logical, Optional            :: isDelete
!! -------------------------------------------------------------------------- !!
    Logical :: isExist_, isDelete_
    Character(len=len(dirPath)) :: convPath

    isDelete_ = .TRUE.
    if ( len_trim(dirPath).GE.1 ) Then

        Call ConvertPathOS( dirPath, convPath )

        !!... Check the directory exists
        isExist_ = IsExistDir( convPath )

        If ( isExist_ ) then
#ifdef __OS_LINUX__
            Call System( "rm -rf "//trim(convPath) )
#else
            Call System( "del /s "//trim(convPath) )
#endif
            !!... Check the directory exists
            isExist_ = IsExistDir( convPath )
        end if

        isDelete_ = .NOT.isExist_

    end if

    If ( Present(isDelete) ) isDelete = isDelete_

!! -------------------------------------------------------------------------- !!
    End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
    Subroutine DeleteFile( filePath, isDelete )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: filePath
    Logical, Optional            :: isDelete
!! -------------------------------------------------------------------------- !!
    Logical :: isExist_, isDelete_
    Character(len=len(filePath)) :: convPath

    isDelete_ = .TRUE.
    if ( len_trim(filePath).GE.1 ) Then

        Call ConvertPathOS( filePath, convPath )

        !!... Check the directory exists
        isExist_ = IsExistFile( convPath )

        If ( isExist_ ) then
#ifdef __OS_LINUX__
            Call System( "rm -rf "//trim(convPath) )
#else
            Call System( "del /s /q "//trim(convPath) )
#endif
            !!... Check the directory exists
            isExist_ = IsExistFile( convPath )
        end if

        isDelete_ = .NOT.isExist_

    end if

    If ( Present(isDelete) ) isDelete = isDelete_

!! -------------------------------------------------------------------------- !!
    End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
    Subroutine CopyDirFile( orgPath, newPath )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: orgPath
    Character(len=*), intent(in) :: newPath
!! -------------------------------------------------------------------------- !!
    Character(len=len(orgPath)) :: convOrgPath
    Character(len=len(newPath)) :: convNewPath

    if ( len_trim(orgPath).LE.0 ) Then
        write(*, "(a)") "[ERROR] CopyDirFile"
        write(*, "(a)") "  Given 'orgPath' is blank."
        write(*,*)
        stop
    end if

    if ( len_trim(newPath).LE.0 ) Then
        write(*, "(a)") "[ERROR] CopyDirFile"
        write(*, "(a)") "  Given 'newPath' is blank."
        write(*,*)
        stop
    end if

    Call ConvertPathOS( orgPath, convOrgPath )
    Call ConvertPathOS( newPath, convNewPath )

    If ( IsExistDir( orgPath ).OR.IsExistFile( orgPath ) ) then
#ifdef __OS_LINUX__
        Call System( "cp -rf "//trim(convOrgPath)//" "//trim(convNewPath) )
#else
        Call System( "xcopy  /Y "//trim(convOrgPath)//" "//trim(convNewPath) )
#endif
    else
        write(*, "(a)") "[ERROR] CopyDirFile"
        write(*, "(a)") "  Given 'orgPath' exist not."
        write(*,*)
        stop
    end if

!! -------------------------------------------------------------------------- !!
    End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
    Subroutine RenameDirFile( orgPath, newPath )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: orgPath
    Character(len=*), intent(in) :: newPath
!! -------------------------------------------------------------------------- !!
    Character(len=len(orgPath)) :: convOrgPath
    Character(len=len(newPath)) :: convNewPath

    if ( len_trim(orgPath).LE.0 ) Then
        write(*, "(a)") "[ERROR] RenameDirFile"
        write(*, "(a)") "  Given 'orgPath' is blank."
        write(*,*)
        stop
    end if

    if ( len_trim(newPath).LE.0 ) Then
        write(*, "(a)") "[ERROR] RenameDirFile"
        write(*, "(a)") "  Given 'newPath' is blank."
        write(*,*)
        stop
    end if

    Call ConvertPathOS( orgPath, convOrgPath )
    Call ConvertPathOS( newPath, convNewPath )

    If ( IsExistDir( orgPath ).OR.IsExistFile( orgPath ) ) then
#ifdef __OS_LINUX__
        Call System( "mv "//trim(convOrgPath)//" "//trim(convNewPath) )
#else
        Call System( "rename "//trim(convOrgPath)//" "//trim(convNewPath) )
#endif
    else
        write(*, "(a)") "[ERROR] RenameDirFile"
        write(*, "(a)") "  Given 'orgPath' exist not."
        write(*,*)
        stop
    end if

!! -------------------------------------------------------------------------- !!
    End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Function IsExistDir( dirPath ) result( isExist )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: dirPath
    Logical                      :: isExist
!! -------------------------------------------------------------------------- !!
    Character(len=len(dirPath)) :: convPath

    if ( len_trim(dirPath).LE.0 ) Then
        write(*, "(a)") "[ERROR] IsExistDir"
        write(*, "(a)") "  Given 'dirPath' is blank."
        write(*,*)
        stop
    end if

    Call ConvertPathOS( dirPath, convPath )

#ifdef __INTEL_FORTRAN__
        Inquire( directory = trim(convPath), exist = isExist )
#else

#ifdef __OS_LINUX__
        Inquire( file = trim(convPath)//"/.", exist = isExist )
#else
        Inquire( file = trim(convPath)//"\.", exist = isExist )
#endif

#endif

!! -------------------------------------------------------------------------- !!
End Function
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Function IsExistFile( filePath ) result( isExist )
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: filePath
    Logical                      :: isExist
!! -------------------------------------------------------------------------- !!
    Character(len=len(filePath)) :: convPath

    if ( len_trim(filePath).LE.0 ) Then
        write(*, "(a)") "[ERROR] IsExistFile"
        write(*, "(a)") "  Given 'filePath' is blank."
        write(*,*)
        stop
    end if

    Call ConvertPathOS( filePath, convPath )

#ifdef __INTEL_FORTRAN__
        Inquire( file = trim(convPath), exist = isExist )
#else
        Inquire( file = trim(convPath), exist = isExist )
#endif

!! -------------------------------------------------------------------------- !!
End Function
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine ConvertPathOS(inputPath, outPath)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Character(len=*), intent(in)               :: inputPath
    Character(len=len(inputPath)), intent(out) :: outPath
!! -------------------------------------------------------------------------- !!
    Integer :: iChar

    do iChar = 1, len(inputPath)
#ifdef __OS_LINUX__
        if ( inputPath(iChar:iChar).EQ.'\' ) then
            outPath(iChar:iChar) = '/'
            else
            outPath(iChar:iChar) = inputPath(iChar:iChar)
        end if
#else
        if ( inputPath(iChar:iChar).EQ.'/' ) then
            outPath(iChar:iChar) = '\'
            else
            outPath(iChar:iChar) = inputPath(iChar:iChar)
        end if
#endif
    end do    
    
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!