!! -------------------------------------------------------------------------- !!
SUBROUTINE SolveODE_Rosenbrock5( &
    &   y, dydx, x, htry, eps, yscal, hdid, hnext, ptrFuncODE, Jacobn)
    !USE nrtype; USE nrutil, ONLY : diagadd,nrerror
    !USE nr, ONLY : lubksb,ludcmp
!! -------------------------------------------------------------------------- !!
!   Fourth order Rosenbrock step for integrating stiff ODEs with monitoring of
!   local truncation error to adjust stepsize.
!! -------------------------------------------------------------------------- !!
    IMPLICIT NONE
    REAL(RP), DIMENSION(:), INTENT(INOUT) :: y
    REAL(RP), DIMENSION(:), INTENT(IN) :: dydx,yscal
    REAL(RP), INTENT(INOUT) :: x
    REAL(RP), INTENT(IN)    :: htry, eps
    REAL(RP), INTENT(OUT)   :: hdid, hnext
    Class(typFuncTypeODE), Pointer      :: ptrFuncODE
    Interface
        SUBROUTINE Jacobn(x, y, dfdx, dfdy)
            Import RP
            IMPLICIT NONE
            REAL(RP), INTENT(IN) :: x
            REAL(RP), DIMENSION(:), INTENT(IN) :: y
            REAL(RP), DIMENSION(:), INTENT(OUT) :: dfdx
            REAL(RP), DIMENSION(:,:), INTENT(OUT) :: dfdy
        END SUBROUTINE Jacobn
    End Interface
!! -------------------------------------------------------------------------- !!
    INTEGER, PARAMETER :: MAXTRY=40
    REAL(RP), PARAMETER :: SAFETY=0.9_RP,GROW=1.5_RP,PGROW=-0.25_RP,&
    SHRNK=0.5_RP,PSHRNK=-1.0_RP/3.0_RP,ERRCON=0.1296_RP,&
    GAM=1.0_RP/2.0_RP,&
    A21=2.0_RP,A31=48.0_RP/25.0_RP,A32=6.0_RP/25.0_RP,C21=-8.0_RP,&
    C31=372.0_RP/25.0_RP,C32=12.0_RP/5.0_RP,&
    C41=-112.0_RP/125.0_RP,C42=-54.0_RP/125.0_RP,&
    C43=-2.0_RP/5.0_RP,B1=19.0_RP/9.0_RP,B2=1.0_RP/2.0_RP,&
    B3=25.0_RP/108.0_RP,B4=125.0_RP/108.0_RP,E1=17.0_RP/54.0_RP,&
    E2=7.0_RP/36.0_RP,E3=0.0_RP,E4=125.0_RP/108.0_RP,&
    C1X=1.0_RP/2.0_RP,C2X=-3.0_RP/2.0_RP,C3X=121.0_RP/50.0_RP,&
    C4X=29.0_RP/250.0_RP,A2X=1.0_RP,A3X=3.0_RP/5.0_RP
    INTEGER :: jtry,ndum
    INTEGER, DIMENSION(size(y)) :: indx
    REAL(RP), DIMENSION(size(y)) :: dfdx,dytmp,err,g1,g2,g3,g4,ysav
    REAL(RP), DIMENSION(size(y),size(y)) :: a,dfdy
    REAL(RP) :: d,errmax,h,xsav
    Logical :: isError
!! -------------------------------------------------------------------------- !!

    Call AssertEq( size(y), size(dydx), size(yscal), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_Rosenbrock5', &
        &   msg  = "Given 'y', 'dydx', 'yscal' size are not match." )
    end if

    xsav = x
    ysav(:) = y(:)

    call Jacobn(xsav, ysav, dfdx, dfdy)

    h = htry
    do jtry = 1, MAXTRY

        a(:,:)=-dfdy(:,:)
        call diagadd( a, 1.0_RP / (GAM*h) )

        call ludcmp(a,indx,d)
        g1=dydx+h*C1X*dfdx

        call lubksb(a,indx,g1)
        y=ysav+A21*g1
        x=xsav+A2X*h

        Call ptrFuncODE%GetDyDx( x, y, dytmp )

        g2=dytmp+h*C2X*dfdx+C21*g1/h
        call lubksb(a,indx,g2)

        y=ysav+A31*g1+A32*g2
        x=xsav+A3X*h

        Call ptrFuncODE%GetDyDx( x, y, dytmp )

        g3=dytmp+h*C3X*dfdx+(C31*g1+C32*g2)/h
        call lubksb(a, indx, g3)

        g4=dytmp+h*C4X*dfdx+(C41*g1+C42*g2+C43*g3)/h
        call lubksb(a, indx, g4)
        y=ysav+B1*g1+B2*g2+B3*g3+B4*g4
        err=E1*g1+E2*g2+E3*g3+E4*g4
        x=xsav+h

        if ( dabs(x-xsav).le.1.d-30 ) then
            Call GURU%Error( &
            &   head = "SolveODE_Rosenbrock5", &
            &   msg  = "stepsize not significant.")
        end if

        errmax = maxval( dabs(err/(yscal+SMALL) ) ) / eps

        if (errmax <= 1.0) then
            hdid  = h
            hnext = merge(SAFETY*h*errmax**PGROW, GROW*h, errmax > ERRCON)
            RETURN
        else
            hnext = SAFETY*h*errmax**PSHRNK
            h=sign(max(abs(hnext),SHRNK*abs(h)),h)
        end if

    end do

    Call GURU%Error( &
    &   head = "SolveODE_Rosenbrock5", &
    &   msg  = "exceeded MAXTRY.")

!! -------------------------------------------------------------------------- !!
Contains
!! -------------------------------------------------------------------------- !!

    ! SUBROUTINE Jacobn(x, y, dfdx, dfdy)
    !     IMPLICIT NONE
    !     REAL(RP), INTENT(IN) :: x
    !     REAL(RP), DIMENSION(:), INTENT(IN) :: y
    !     REAL(RP), DIMENSION(:), INTENT(OUT) :: dfdx
    !     REAL(RP), DIMENSION(:,:), INTENT(OUT) :: dfdy
    !         dfdx(:)   =  0.0_RP
    !         dfdy(1,1) = -0.013_RP - 1000.0_RP*y(3)
    !         dfdy(1,2) =  0.0_RP
    !         dfdy(1,3) = -1000.0_RP*y(1)
    !         dfdy(2,1) =  0.0_RP
    !         dfdy(2,2) = -2500.0_RP*y(3)
    !         dfdy(2,3) = -2500.0_RP*y(2)
    !         dfdy(3,1) = -0.013_RP - 1000.0_RP*y(3)
    !         dfdy(3,2) = -2500.0_RP*y(3)
    !         dfdy(3,3) = -1000.0_RP*y(1) - 2500.0_RP*y(2)
    ! END SUBROUTINE Jacobn

    SUBROUTINE ludcmp(a, indx, d)
        Implicit None
        Real(RP), dimension(:, :), intent(inout) :: a
        Integer, dimension(:)                    :: indx
        Real(RP)                                 :: d
        !...
        Real(RP), Parameter :: TINY=1.D-20
        Integer  :: n
        INTEGER  :: i, imax, j, k
        Real(RP) :: aamax, dum, sum
        Real(RP), dimension(size(a, 1)) :: vv

        if ( size(a, 1).ne.size(a, 2) ) then
            Call GURU%Error( &
            &   head = "ludcmp", &
            &   msg  = "Given 'a' should be square matrix." )
        end if

        if ( size(a, 1).ne.size(indx, 1) ) then
            Call GURU%Error( &
            &   head = "ludcmp", &
            &   msg  = "Given 'a' and 'indx' should have the same number of rows and length." )
        end if

        n = size(a, 1)

        d = 1.0_RP
        do i=1,n
            aamax = 0.0_RP
            do j = 1,n
                if ( dabs(a(i,j)).GT.aamax) aamax = dabs(a(i,j))
            end do
            if ( dabs(aamax).LE.1.D-30 ) then
                Call GURU%Error( &
                &   head = "ludcmp", &
                &   msg  = "Given 'a' is singular matrix." )
            end if
            vv(i) = 1.0_RP / aamax
        end do

        do j = 1, n
            do i = 1, j-1
                sum = a( i, j )
                do k =1 , i-1
                    sum = sum - a(i,k)*a(k,j)
                enddo
                a(i,j) = sum
            enddo
            aamax=0.0_RP

            do i = j, n
               sum = a(i,j)
               do k = 1, j-1
                sum = sum - a(i,k)*a(k,j)
               end do
               a(i, j) = sum
               dum = vv(i) * dabs(sum)
               if (dum.ge.aamax) then
                    imax = i
                    aamax = dum
               endif
            enddo

            if ( j.ne.imax ) then
                do k = 1, n
                    dum = a(imax,k)
                    a(imax,k) = a(j,k)
                    a(j,k) = dum
                enddo
                d = -d
                vv(imax) = vv(j)
            endif

            indx(j) = imax
            if( dabs(a(j,j)).LE.1.D-30 ) a(j,j) = TINY

            if ( j.ne.n )then
                dum = 1.0_RP / a(j,j)
                do i = j+1, n
                    a(i,j) = a(i,j) * dum
                enddo
            endif
        end do

    End Subroutine

    Subroutine lubksb(a, indx, b)
        Implicit None
        Real(RP), dimension(:, :), intent(inout) :: a
        Integer, dimension(:)                    :: indx
        Real(RP), dimension(size(a,1))            :: b
        !...
        INTEGER :: i, ii, j, ll, n
        Real(RP) :: sum

        if ( size(a, 1).ne.size(a, 2) ) then
            Call GURU%Error( &
            &   head = "ludcmp", &
            &   msg  = "Given 'a' should be square matrix." )
        end if

        if ( size(a, 1).ne.size(indx, 1) ) then
            Call GURU%Error( &
            &   head = "ludcmp", &
            &   msg  = "Given 'a' and 'indx' should have the same number of rows and length." )
        end if

        n = size(a, 1)

        ii = 0

        do i = 1, n
            ll  = indx(i)
            sum = b(ll)
            b(ll) = b(i)
            if ( ii.ne.0 ) then
                do j = ii, i-1
                    sum = sum - a(i,j)*b(j)
                enddo
            else if ( dabs(sum).le.1.d-30 ) then
                ii=i
            end if
            b(i) = sum
        end do

        do i = n, 1, -1
            sum = b(i)
            do j = i+1, n
                sum = sum - a(i,j)*b(j)
            enddo
            b(i) = sum / a(i,i)

        enddo

    End Subroutine

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!