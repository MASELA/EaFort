!! -------------------------------------------------------------------------- !!
Type, Abstract :: typFuncTypeODE
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Contains
!! -------------------------------------------------------------------------- !!

    !!... Get DyDx Real
    Procedure, Pass :: GetDyDxReal => GetDyDxReal_typFuncTypeODE

    !!... Get DyDx Cmplx
    Procedure, Pass :: GetDyDxCmplx => GetDyDxCmplx_typFuncTypeODE

    !!... Generic Subroutine
    Generic :: GetDyDx => GetDyDxReal, GetDyDxCmplx

!! -------------------------------------------------------------------------- !!
End Type
!! -------------------------------------------------------------------------- !!