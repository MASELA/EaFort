!! -------------------------------------------------------------------------- !!
Subroutine GetDyDxReal_typFuncTypeODE(this, x, y, dydx)
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Class(typFuncTypeODE), intent(inout) :: this
    Real(RP), intent(in)                 :: x
    Real(RP), Dimension(:), intent(in)   :: y
    Real(RP), Dimension(:), intent(out)  :: dydx
!! -------------------------------------------------------------------------- !!
    Call GURU%Error( &
    &   head = "GetDyDxReal_typFuncTypeODE", &
    &   msg  = "Abstract type is called. ")
    dydx = 0.0_RP
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine GetDyDxCmplx_typFuncTypeODE(this, x, y, dydx)
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Class(typFuncTypeODE), intent(inout)   :: this
    Real(RP), intent(in)                   :: x
    Complex(CP), Dimension(:), intent(in)  :: y
    Complex(CP), Dimension(:), intent(out) :: dydx
!! -------------------------------------------------------------------------- !!
    Call GURU%Error( &
    &   head = "GetDyDxCmplx_typFuncTypeODE", &
    &   msg  = "Abstract type is called. ")
    dydx = dcmplx(0.d0, 0.d0)
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!