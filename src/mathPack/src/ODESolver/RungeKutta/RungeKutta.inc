!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_RK4_Real(y, dydx, x, h, yout, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), Dimension(:), Intent(in)  :: y, dydx
    Real(RP), intent(in)                :: x, h
    Real(RP), Dimension(:), intent(out) :: yout
    Class(typFuncTypeODE), Pointer      :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer  :: ndum
    Real(RP) :: h6, hh, xh
    Real(RP), dimension(size(y)) :: dym, dyt, yt
    Logical :: isError
!! -------------------------------------------------------------------------- !!
    Call AssertEq( size(y), size(dydx), size(yout), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_RK4_Real', &
        &   msg  = "Given 'y', 'dydx', 'yout' size are not match." )
    end if
    hh = h * 0.5_RP
    h6 = h / 6.0_RP

    xh = x + hh
    yt(:) = y(:) + hh * dydx(:)
    Call ptrFuncODE%GetDyDx( xh, yt, dyt )

    yt(:) = y(:) + hh * dyt(:)
    Call ptrFuncODE%GetDyDx( xh, yt, dym )

    yt(:)  = y + h * dym(:)
    dym(:) = dyt(:) + dym(:)
    Call ptrFuncODE%GetDyDx( x + h, yt, dyt )
    yout(:) = y(:) + h6 * (dydx(:) + dyt(:) + 2.0_RP * dym(:) )

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!


!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_RK4_Complex(y, dydx, x, h , yout, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Complex(CP), Dimension(:), Intent(in)  :: y, dydx
    Real(RP), intent(in)                   :: x, h
    Complex(CP), Dimension(:), intent(out) :: yout
    Class(typFuncTypeODE), Pointer         :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer  :: ndum
    Real(RP) :: h6, hh, xh
    Complex(CP), dimension(size(y)) :: dym, dyt, yt
    Logical                         :: isError
!! -------------------------------------------------------------------------- !!

    Call AssertEq( size(y), size(dydx), size(yout), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_RK4_Complex', &
        &   msg  = "Given 'y', 'dydx', 'yout' size are not match." )
    end if
    hh = h * 0.5_RP
    h6 = h / 6.0_RP

    xh    = x    + hh
    yt(:) = y(:) + hh * dydx(:)
    Call ptrFuncODE%GetDyDx( xh, yt, dyt )

    yt(:) = y(:) + hh * dyt(:)
    Call ptrFuncODE%GetDyDx( xh, yt, dym )

    yt(:)  = y(:)   + h * dym(:)
    dym(:) = dyt(:) + dym(:)

    Call ptrFuncODE%GetDyDx( x + h, yt, dyt )
    yout(:) = y(:) + h6 * (dydx(:) + dyt(:) + 2.0_RP * dym(:) )

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_AdaptiveRK5_Real( &
    & y, dydx, x, hTry, errorBound, ysCal, hdid, hNext, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), Dimension(:), Intent(inout) :: y
    Real(RP), Dimension(:), Intent(in)    :: dydx, ysCal
    Real(RP), intent(inout)               :: x
    Real(RP), intent(in)                  :: hTry, errorBound
    Real(RP), intent(out)                 :: hdid, hNext
    Class(typFuncTypeODE), Pointer        :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer :: nDum
    Real(RP) :: errMax, h, hTemp, xNew
    Real(RP), Dimension( size(y) ) :: yErr, yTemp
    Real(RP), Parameter :: SAFETY = 0.9_RP, PGROW = -0.2_RP, PSHRINK = -0.25_RP
    Real(RP), Parameter :: ERRCON = 1.89D-4
    Logical             :: isError
!! -------------------------------------------------------------------------- !!
    Call AssertEq( size(y), size(dydx), size(ysCal), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_AdaptiveRK5_Real', &
        &   msg  = "Given 'y', 'dydx', 'ysCal' size are not match." )
    end if

    !... Set Stepsize to the initial trial value
    h = hTry

    subStep: Do

        !...Take a Step
        Call SolveODE_RK5CashKarp( y, dydx, x, h, yTemp, yErr, ptrFuncODE )

        !... Evaluate the accuracy
        errMax = maxval( dabs(yErr(:)/(ysCal(:) + SMALL) ) ) / errorBound


        if ( errMax.LE. 1.0_RP ) exit subStep

        !... Truncation error too large, reduce stepsize
        hTemp = SAFETY * h * (errMax**PSHRINK)

        !... No more than a factor of 10
        h = dsign( max(dabs(hTemp), 0.1_RP * dabs(h) ), h )

        xNew = x + h
        if ( dabs(xNew-x).LE.1.D-12 ) then
            Call GURU%Error( &
            &   head = "SolveODE_AdaptiveRK5_Real", &
            &   msg  = "Stepsize underflow.")
        end if

    End do subStep

    if ( errMax > ERRCON ) then
        hNext = SAFETY * h * (errMax**PGROW)
    else
        hNext = 5.0_RP * h
    end if

    hDid = h
    x    = x + h

    y(:) = yTemp(:)

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_RK5CashKarp_Real(y, dydx, x, h, yOut, yErr, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), Dimension(:), Intent(in)  :: y, dydx
    Real(RP), intent(inout)             :: x, h
    Real(RP), Dimension(:), intent(out) :: yOut, yErr
    Class(typFuncTypeODE), Pointer      :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer :: nDum
    Real(RP), Dimension(size(y)) :: ak2, ak3, ak4, ak5, ak6, yTemp
    Real(RP), Parameter :: &
    &   A2 =0.2_RP, A3 =0.3_RP, A4=0.6_RP, A5=1.0_RP, A6=0.875_RP,          &
    &   B21=0.2_RP, B31=3.0_RP/40.0_RP,   B32=9.0_RP/40.0_RP,               &
    &   B41=0.3_RP, B42=-0.9_RP,          B43=1.2_RP,                       &
    &   B51=-11.0_RP/54.0_RP, B52=2.5_RP,               B53=-70.0_RP/27.0_RP,  &
    &   B54=35.0_RP/27.0_RP,  B61=1631.0_RP/55296.0_RP, B62=175.0_RP/512.0_RP, &
    &   B63=575.0_RP/13824.0_RP, B64=44275.0_RP/110592.0_RP, B65=253.0_RP/4096.0_RP, &
    &   C1=37.0_RP/378.0_RP,  C3=250.0_RP/621.0_RP, &
    &   C4=125.0_RP/594.0_RP, C6=512.0_RP/1771.0_RP, &
    &   DC1=C1-2825.0_RP/27648.0_RP,  DC3=C3-18575.0_RP/48384.0_RP, &
    &   DC4=C4-13525.0_RP/55296.0_RP, DC5=-277.0_RP/14336.0_RP, DC6=C6-0.25_RP
    Logical :: isError
!! -------------------------------------------------------------------------- !!

    Call AssertEq( size(y), size(dydx), size(yOut), ndum, isError )

    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_RK5CashKarp_Real', &
        &   msg  = "Given 'y', 'dydx', 'yOut' size are not match." )
    end if

    yTemp(:) = y(:) + B21 * h * dydx(:)
    Call ptrFuncODE%GetDyDx( x+A2*h, yTemp, ak2 )

    yTemp(:) = y(:) + h * ( B31*dydx(:) + B32*ak2(:) )
    Call ptrFuncODE%GetDyDx( x+A3*h, yTemp, ak3 )

    yTemp(:) = y(:) + h * ( B41*dydx(:) + B42*ak2(:) + B43*ak3(:) )
    Call ptrFuncODE%GetDyDx( x+A4*h, yTemp, ak4 )

    yTemp(:) = y(:) + h * ( B51*dydx(:) + B52*ak2(:) + B53*ak3(:) + B54*ak4(:) )
    Call ptrFuncODE%GetDyDx( x+A5*h, yTemp, ak5 )

    yTemp(:) = y(:) + h * ( B61*dydx(:) + B62*ak2(:) + B63*ak3(:) + B64*ak4(:) + B65*ak5(:) )
    Call ptrFuncODE%GetDyDx( x+A6*h, yTemp, ak6 )

    yOut(:) = y(:) + h * (C1*dydx(:) + C3*ak3(:) + C4*ak4(:) + C6*ak6(:) )
    yErr(:) = h*( DC1*dydx(:) + DC3*ak3(:) + DC4*ak4(:) + DC5*ak5(:) + DC6*ak6(:) )

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!