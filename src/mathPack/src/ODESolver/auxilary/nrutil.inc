FUNCTION upper_triangle(j,k,extra)
    ! Return an upper triangular logical mask.
    INTEGER, INTENT(IN) :: j,k
    INTEGER, OPTIONAL, INTENT(IN) :: extra
    LOGICAL, DIMENSION(j,k) :: upper_triangle
    INTEGER :: n
    n=0
    if (present(extra)) n=extra
    upper_triangle=(outerdiff(arth_i(1,1,j),arth_i(1,1,k)) < n)
END FUNCTION upper_triangle

FUNCTION outerdiff_r(a,b)
    REAL(RP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(RP), DIMENSION(size(a),size(b)) :: outerdiff_r
    outerdiff_r = spread(a,dim=2,ncopies=size(b)) - &
    spread(b,dim=1,ncopies=size(a))
END FUNCTION outerdiff_r

FUNCTION outerdiff_i(a,b)
    INTEGER, DIMENSION(:), INTENT(IN) :: a,b
    INTEGER, DIMENSION(size(a),size(b)) :: outerdiff_i
    outerdiff_i = spread(a,dim=2,ncopies=size(b)) - &
    spread(b,dim=1,ncopies=size(a))
END FUNCTION outerdiff_i

FUNCTION outerprod_r(a,b)
    REAL(RP), DIMENSION(:), INTENT(IN) :: a,b
    REAL(RP), DIMENSION(size(a),size(b)) :: outerprod_r
    outerprod_r = spread(a,dim=2,ncopies=size(b)) * &
    spread(b,dim=1,ncopies=size(a))
END FUNCTION outerprod_r


FUNCTION arth_r(first,increment,n)
    REAL(RP), INTENT(IN) :: first,increment
    INTEGER, INTENT(IN) :: n
    REAL(RP), DIMENSION(n) :: arth_r
    INTEGER :: k,k2
    REAL(RP) :: temp
    if (n > 0) arth_r(1)=first
    if (n <= NPAR_ARTH) then
    do k=2,n
    arth_r(k)=arth_r(k-1)+increment
    end do
    else
    do k=2,NPAR2_ARTH
    arth_r(k)=arth_r(k-1)+increment
    end do
    temp=increment*NPAR2_ARTH
    k=NPAR2_ARTH
    do
    if (k >= n) exit
    k2=k+k
    arth_r(k+1:min(k2,n))=temp+arth_r(1:min(k,n-k))
    temp=temp+temp
    k=k2
    end do
    end if
END FUNCTION arth_r


FUNCTION arth_i(first,increment,n)
    INTEGER, INTENT(IN) :: first,increment,n
    INTEGER, DIMENSION(n) :: arth_i
    INTEGER :: k,k2,temp
    if (n > 0) arth_i(1)=first
    if (n <= NPAR_ARTH) then
    do k=2,n
    arth_i(k)=arth_i(k-1)+increment
    end do
    else
    do k=2,NPAR2_ARTH
    arth_i(k)=arth_i(k-1)+increment
    end do
    temp=increment*NPAR2_ARTH
    k=NPAR2_ARTH
    do
    if (k >= n) exit
    k2=k+k
    arth_i(k+1:min(k2,n))=temp+arth_i(1:min(k,n-k))
    temp=temp+temp
    k=k2
    end do
    end if
END FUNCTION arth_i


Recursive Subroutine CumSum_Real(arr, ans, seed)
    Implicit None
    Integer, parameter :: NPAR_CUMSUM = 16
    Real(RP), dimension(:), intent(in)          :: arr
    Real(RP), dimension(size(arr)), intent(out) :: ans
    Real(RP), Optional, intent(in)              :: seed
    Integer :: n, j
    Real(RP) :: sd
    n = size(arr)
    if (n.le.0) return
    sd = 0.0_RP
    if (present(seed)) sd = seed
    ans(1) = arr(1) + seed
    if ( n < NPAR_CUMSUM ) then
        do j = 2, n
            ans(j) = ans(j-1) + ans(j)
        end do
    else
        Call CumSum_Real( arr(2:n:2)+arr(1:n-1:2), ans(2:n:2), sd )
        ans(3:n:2) = ans(2:n-1:2) + arr(3:n:2)
    end if

End Subroutine

Recursive Subroutine CumSum_Int(arr, ans, seed)
    Implicit None
    Integer, parameter :: NPAR_CUMSUM = 16
    Integer, dimension(:), intent(in)           :: arr
    Integer, dimension(size(arr)), intent(out)  :: ans
    Integer, Optional, intent(in)               :: seed
    Integer :: n, j
    Integer :: sd

    n = size(arr)
    if (n.le.0) return
    sd = 0
    if (present(seed)) sd = seed
    ans(1) = arr(1) + seed
    if ( n < NPAR_CUMSUM ) then
        do j = 2, n
            ans(j) = ans(j-1) + ans(j)
        end do
    else
        Call CumSum_Int( arr(2:n:2)+arr(1:n-1:2), ans(2:n:2), sd )
        ans(3:n:2) = ans(2:n-1:2) + arr(3:n:2)
    end if

End Subroutine

Subroutine AssertEq2(n1, n2, nOut, isError)
    Implicit None
    Integer, intent(in)  :: n1, n2
    Integer, intent(out) :: nOut
    Logical, intent(out) :: isError
    isError = .FALSE.
    if (n1.eq.n2) then
        nOut = n1
    else
        nOut = 0
        isError = .TRUE.
    end if
End Subroutine

Subroutine AssertEq3(n1, n2, n3, nOut, isError)
    Implicit None
    Integer, intent(in)  :: n1, n2, n3
    Integer, intent(out) :: nOut
    Logical, intent(out) :: isError
    isError = .FALSE.
    if ( (n1.eq.n2).AND.(n2.EQ.n3) ) then
        nOut = n1
    else
        nOut = 0
        isError = .TRUE.
    end if
End Subroutine

Subroutine AssertEq4(n1, n2, n3, n4, nOut, isError)
    Implicit None
    Integer, intent(in)  :: n1, n2, n3, n4
    Integer, intent(out) :: nOut
    Logical, intent(out) :: isError
    isError = .FALSE.
    if ( (n1.eq.n2).AND.(n2.EQ.n3).AND.(n3.eq.n4) ) then
        nOut = n1
    else
        nOut = 0
        isError = .TRUE.
    end if
End Subroutine

FUNCTION iminloc(arr)   !Index of minloc on an array.
    REAL(RP), DIMENSION(:), INTENT(IN) :: arr
    INTEGER, DIMENSION(1) :: imin
    INTEGER :: iminloc
    imin    = minloc(arr(:))
    iminloc = imin(1)
END FUNCTION iminloc

SUBROUTINE diagadd_rv(mat,diag)
    ! Adds vector or scalar diag to the diagonal of matrix mat.
    REAL(RP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(RP), DIMENSION(:), INTENT(IN) :: diag
    INTEGER :: j,n
    Logical :: isError
    Call AssertEq( size(diag), min( size(mat,1),size(mat,2) ), n, isError)
    If (isError) then
        Call GURU%Error( &
        &   head = "diagadd_rv", &
        &   msg  = "The size of 'diag', 'mat's row and column are different ")
    end if
    do j=1,n
        mat(j,j)=mat(j,j)+diag(j)
    end do
END SUBROUTINE diagadd_rv

SUBROUTINE diagadd_r(mat,diag)
    REAL(RP), DIMENSION(:,:), INTENT(INOUT) :: mat
    REAL(RP), INTENT(IN) :: diag
    INTEGER :: j,n

    n = min( size(mat,1),size(mat,2) )
    do j=1,n
        mat(j,j)=mat(j,j)+diag
    end do

END SUBROUTINE diagadd_r
