!! -------------------------------------------------------------------------- !!
Type typSolveODE_BulirschStoer
!! -------------------------------------------------------------------------- !!

    Private

    Integer :: IMAX = 9

    Integer :: KMAXX = 8

    Real(RP) :: SAFE1=0.25_RP, SAFE2=0.7_RP, REDMAX=1.D-5

    Real(RP) :: REDMIN=0.7_RP, TINY=1.D-30, SCALMX=0.1_RP

    Integer, Dimension(9) :: NSEQ = [2, 4, 6, 8, 10, 12, 14, 16, 18]

    Integer :: kopt, kmax

    Real(RP), Dimension(8, 8) :: alf

    Real(RP), Dimension(8) :: err

    Integer, Dimension(9) :: a

    Real(RP) :: epsOld = -1.0_RP, xNew

    Logical :: isFirst = .TRUE.

    !!... Variables assciciated with pzextr
    Integer :: IEST_MAX = 16

    Integer :: nvold = 0

    Real(RP), Dimension(16) :: x_pzextr

    REAL(RP), allocatable :: qcol(:,:)

!! -------------------------------------------------------------------------- !!
Contains
!! -------------------------------------------------------------------------- !!

    Procedure :: SolveODE => SolveODE_typSolveODE_BulirschStoer

    Procedure, Private :: pzextr => pzextr_typSolveODE_BulirschStoer

!! -------------------------------------------------------------------------- !!
End Type
!! -------------------------------------------------------------------------- !!

Interface SolveODE_ModifiedMidpoint
    Procedure :: SolveODE_ModifiedMidpoint_Real
End Interface