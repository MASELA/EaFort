!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_ModifiedMidpoint_Real(y, dydx, xs, hTot, nStep, yOut, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), Dimension(:), Intent(in)  :: y, dydx
    Real(RP), intent(in)                :: xs, hTot
    Integer, intent(in)                 :: nStep
    Real(RP), Dimension(:), Intent(out) :: yOut
    Class(typFuncTypeODE), Pointer      :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer  :: nDum, n
    Real(RP) :: h, h2, x
    Real(RP), Dimension(size(y)) :: ym, yn, ySwap
    Logical :: isError
!! -------------------------------------------------------------------------- !!
    Call AssertEq( size(y), size(dydx), size(yout), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_ModifiedMidpoint_Real', &
        &   msg  = "Given 'y', 'dydx', 'yout' size are not match." )
    end if

    h     = hTot / real(nStep, RP)
    ym(:) = y(:)
    yn(:) = y(:) + h * dydx(:)

    x = xs + h
    Call ptrFuncODE%GetDyDx( x, yn, yOut )

    h2 = 2.0_RP * h
    do n = 2, nStep
        ySwap(:) = ym(:)
        ym(:)    = yn(:)
        yn(:)    = ySwap(:)

        yn(:) = yn(:) + h2 * yOut
        x = x + h
        Call ptrFuncODE%GetDyDx( x, yn, yOut )

    End do
    yOut(:) = 0.5_RP * ( ym(:) + yn(:) + h * yOut(:) )
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!


!! -------------------------------------------------------------------------- !!
Subroutine SolveODE_typSolveODE_BulirschStoer( this, &
    &   y, dydx, x, hTry, errorBound, ysCal, hDid, hNext, ptrFuncODE)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Class(typSolveODE_BulirschStoer)      :: this
    Real(RP), Dimension(:), Intent(inout) :: y
    Real(RP), Dimension(:), Intent(in)    :: dydx, ysCal
    Real(RP), intent(inout)               :: x
    Real(RP), intent(in)                  :: hTry, errorBound
    Real(RP), intent(out)                 :: hdid, hNext
    Class(typFuncTypeODE), Pointer        :: ptrFuncODE
!! -------------------------------------------------------------------------- !!
    Integer :: k, km, nDum
    Real(RP) :: eps1, errMax, fact, h, red, scale, wrkmin, xest
    Real(RP), Dimension(size(y)) :: yErr, ySav, yseq
    Logical :: reduct
    Logical :: isError
    Integer :: i, j
    Integer :: kopt_
!! -------------------------------------------------------------------------- !!

    !! FIXME: Please check the 2D problem. YM. 2023-08-03.
    !! It seems that it has a problem with absolute error.

    Call AssertEq( size(y), size(dydx), size(ysCal), ndum, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'SolveODE_BulirschStoer_Real', &
        &   msg  = "Given 'y', 'dydx', 'ysCal' size are not match." )
    end if

    !!... A new tolerance, so re-initialize.
    if ( (dabs(errorBound-this%epsOld).ge.this%TINY).or.this%isFirst ) then

        hNext      = -1.D29
        this%xNew  =  1.D29

        eps1 = this%SAFE1 * errorBound

        this%a(:) = 0.0_RP
        Call CumSum( this%NSEQ, this%a, 1 )

        where ( upper_triangle(this%KMAXX, this%KMAXX) ) &
        & this%alf = eps1** (outerdiff(this%a(2:), this%a(2:)) &
        &                   /outerprod( arth(3.0_RP, 2.0_RP, this%KMAXX), &
        &                               (this%a(2:)-this%a(1)+1.0_RP) ) )

        this%epsOld = errorBound

        Do kopt_ = 2, this%KMAXX-1
            if ( this%a(kopt_+1) > this%a(kopt_)*this%alf(kopt_-1,kopt_) ) exit
        End do

        this%kopt = kopt_
        this%kmax = this%kopt

        this%isFirst = .FALSE.

    end if

    h       = hTry
    ySav(:) = y(:)                      ! Save the starting values.
    if ( dabs(h-hnext).ge.this%TINY .or. dabs(x-this%xNew).ge.this%TINY ) then
        this%isFirst = .true.
        kopt_        = this%kmax
        this%kopt    = this%kmax
    end if
    reduct=.false.

    main_loop: do

        do k = 1, this%kmax

            this%xNew = x + h
            if ( dabs(this%xNew-x).le.this%TINY ) then
                Call GURU%Error( &
                &   head = "SolveODE_BulirschStoer_Real", &
                &   msg  = "Stepsize underflow." )
            end if

            Call SolveODE_ModifiedMidpoint(ySav, dydx, x, h, this%NSEQ(k), yseq, ptrFuncODE)
            xest = ( h / this%NSEQ(k) )**2

            call this%pzextr(k, xest, yseq, y, yerr)

            if (k.NE.1) then    ! Compute normalized error estimate \epsilon(k).
               errMax = maxval( dabs( yerr(:)/(yscal(:) + SMALL) ) )
               errMax = max( this%TINY, errmax ) / errorBound  ! Scale error relative to tolerance.
               km     = k - 1
               this%err(km) = ( errmax/this%SAFE1 )**(1.0_RP/(2.0_RP*km+1.0_RP))
            end if

            if (k.NE.1 .and. (k.GE.kopt_-1 .or. this%isFirst)) then     ! In order window.

                if (errmax.LT.1.0) exit main_loop         ! Converged.

                if ( k.EQ.this%kmax .or. k.EQ.kopt_+1) then    ! Check for possible step-size reduction.

                    red = this%SAFE2 / this%err(km)
                    exit

                else if (k.EQ.kopt_) then

                    if (this%alf(kopt_-1,kopt_) < this%err(km)) then
                        red = 1.0_RP/this%err(km)
                        exit
                    end if

               else if (kopt_.EQ.this%kmax) then

                    if ( this%alf(km, this%kmax-1) < this%err(km) ) then
                        red = this%alf(km, this%kmax-1) * this%SAFE2 / this%err(km)
                        exit
                    end if

               else if ( this%alf(km,kopt_) < this%err(km) ) then
                    red = this%alf(km, kopt_-1) / this%err(km)
                    exit
               end if

            end if

        end do
        red = max( min(red,this%REDMIN), this%REDMAX )
        h   = h * red
        reduct = .true.

    end do main_loop

    x = this%xNew              ! Successful step taken.
    hdid = h
    this%isFirst = .false.
    this%kopt = 1 + iminloc( this%a(2:km+1) * max(this%err(1:km), this%SCALMX) )
    kopt_ = this%kopt

    ! Compute optimal row for convergence and corresponding stepsize.
    scale  = max( this%err(kopt_-1), this%SCALMX )
    wrkmin = scale * this%a(kopt_)
    hNext  = h / scale

    if ( kopt_.GE.k .and. kopt_.NE.this%kmax .and. .not. reduct) then
        fact = max( scale/this%alf(kopt_-1,kopt_), this%SCALMX )
        if ( this%a(kopt_+1) * fact <= wrkmin) then
            hnext     = h/fact
            kopt_     = kopt_ + 1
            this%kopt = kopt_
        end if
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!


!! -------------------------------------------------------------------------- !!
Subroutine pzextr_typSolveODE_BulirschStoer(this, iest, xest, yest, yz, dy)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Class(typSolveODE_BulirschStoer), intent(inout) :: this
    INTEGER, INTENT(IN)  :: iest
    REAL(RP), INTENT(IN) :: xest
    REAL(RP), DIMENSION(:), INTENT(IN) :: yest
    REAL(RP), DIMENSION(:), INTENT(OUT) :: yz,dy
!! -------------------------------------------------------------------------- !!
    INTEGER :: j,nv
    REAL(RP) :: delta,f1,f2
    REAL(RP), DIMENSION(size(yz)) :: d,tmp,q
    Logical :: isError
!! -------------------------------------------------------------------------- !!

    Call AssertEq( size(yz), size(yest), size(dy), nv, isError )
    if (isError) then
        Call GURU%Error( &
        &   head = 'pzextr_typSolveODE_BulirschStoer', &
        &   msg  = "Given 'yz', 'yest', 'dy' size are not match." )
    end if

    if ( iest > this%IEST_MAX ) then
        Call GURU%Error( &
        &   head = 'pzextr_typSolveODE_BulirschStoer', &
        &   msg  = "probable misuse, too much extrapolation." )
    End if

    if ( nv.ne.this%nvold ) then       !   Set up internal storage.
        if ( allocated(this%qcol) ) deallocate(this%qcol)
        allocate( this%qcol(nv, this%IEST_MAX) )
        this%nvold = nv
    end if

    this%x_pzextr(iest) = xest            ! Save current independent variable.
    dy(:)               = yest(:)
    yz(:)               = yest(:)

    if (iest.EQ.1) then       ! Store first estimate in first column.
        this%qcol(:,1)=yest(:)
    else
        d(:)=yest(:)
        do j = 1, iest-1
            delta = 1.0_RP/ ( this%x_pzextr(iest-j) - xest )
            f1    = xest * delta
            f2    = this%x_pzextr(iest-j) * delta
            q(:)  = this%qcol(:,j)        ! Propagate tableau 1 diagonal more.
            this%qcol(:, j) = dy(:)
            tmp(:) = d(:) - q(:)
            dy(:)  = f1 * tmp(:)
            d(:)   = f2 * tmp(:)
            yz(:)  = yz(:) + dy(:)
        end do
        this%qcol(:,iest) = dy(:)
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!