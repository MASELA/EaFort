!! -------------------------------------------------------------------------- !!
Type typSimulatedAnnealing
!! -------------------------------------------------------------------------- !!

    Integer  :: finalReductionNumber = 4

    Real(RP) :: reductionFactor = 0.5_RP

    Real(RP) :: errorTolerance = 1.D-4

    Integer  :: iseed1 = 1, iseed2 = 2

    Integer  :: numberOfCycle = 20

    integer  :: maxFuncEvaluation = 1000000

    Logical  :: logPrint = .FALSE.

!! -------------------------------------------------------------------------- !!
Contains
!! -------------------------------------------------------------------------- !!

    !!... Set Parameters
    Procedure :: SetParameters => SetParameters_typSimulatedAnnealing

    !!... Conduct Optimization
    Procedure :: Optimize => Optimize_typSimulatedAnnealing

!! -------------------------------------------------------------------------- !!
End Type
!! -------------------------------------------------------------------------- !!