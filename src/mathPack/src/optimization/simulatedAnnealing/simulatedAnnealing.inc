!! -------------------------------------------------------------------------- !!
Subroutine SetParameters_typSimulatedAnnealing(this, json)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Class(typSimulatedAnnealing), intent(inout) :: this
    Type(typJSON), Pointer                      :: json
!! -------------------------------------------------------------------------- !!

    Call JSON_GetRealOrDefault( json, "reductionFactor", this%reductionFactor, 0.5_RP)
    Call JSON_GetRealOrDefault( json, "errorTolerance", this%errorTolerance, 1.D-4)

    Call JSON_GetIntOrDefault( json, "seed1", this%iseed1, 1)
    Call JSON_GetIntOrDefault( json, "seed2", this%iseed2, 2)

    Call JSON_GetIntOrDefault( json, "nCycle", this%numberOfCycle, 20)
    Call JSON_GetIntOrDefault( json, "maxEvaluation", this%maxFuncEvaluation, 1000000)

    Call JSON_GetLogicalOrDefault( json, "logPrint", this%logPrint, .FALSE.)

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine Optimize_typSimulatedAnnealing(this, &
    &   ptrOptFunc, nVariable, initialValue, &
    &   lowerBoundVariable, upperBoundVariable, optVariable, optValue, isError )
!! -------------------------------------------------------------------------- !!
    Implicit None
    Logical, Parameter :: FUNCMAX = .FALSE.
    Class(typSimulatedAnnealing), intent(in) :: this
    Class(typOptFunc), Pointer :: ptrOptFunc
    Integer :: nVariable
    Real(RP), Dimension(nVariable) :: initialValue
    Real(RP), Dimension(nVariable) :: lowerBoundVariable
    Real(RP), Dimension(nVariable) :: upperBoundVariable
    Real(RP), Dimension(nVariable) :: optVariable
    Real(RP) :: optValue
    Logical  :: isError
!! -------------------------------------------------------------------------- !!

    isError = .FALSE.

    Block
        Double Precision, Allocatable :: X(:), LB(:), UB(:), C(:)
        Double Precision, Allocatable :: VM(:), FSTAR(:), XOPT(:), XP(:)
        Double Precision :: T, EPS, RT, FOPT
        Integer, Allocatable :: NACP(:)
        Integer :: N, NS, NT, NEPS, NACC, MAXEVL, IPRINT
        Integer :: NOBDS, IER, NFCNEV, ISEED1, ISEED2
        LOGICAL :: MAX
        Double precision :: H

        !!... Set number of variables
        N = nVariable

        !!... Set Variables
        If (Allocated(X)) Deallocate( X ); Allocate( X(N) )
        X(:) = initialValue(:)

        !!... Always Minimize
        MAX = FUNCMAX

        !!... Reduction Factor
        RT = this%reductionFactor

        !!... Error Tolerance
        EPS = this%errorTolerance

        !!... Set Number of Cycle (Suggested : 20)
        NS = this%numberOfCycle

        !!... Number of iterations before temperature reduction
        !!... Value suggested by Corana et al. is MAX(100, 5*N)
        NT = MAX0(100, 5*N)

        !!... Number of final function values used to decide upon
        !!... termination. Suggested value is 4.
        NEPS = 4

        !!... The maximum number of function evaluations.
        MAXEVL = this%maxFuncEvaluation

        !!... The lower bound for the allowable solution variables.
        If (Allocated(LB)) Deallocate( LB ); Allocate( LB(N) )
        LB(:) = lowerBoundVariable(:)

        !!... The upper bound for the allowable solution variables.
        If (Allocated(UB)) Deallocate( UB ); Allocate( UB(N) )
        UB(:) = upperBoundVariable(:)

        !!... Vector that controls the step length adjustment.
        !!... The suggested value for all elements is 2.0.
        !!... The lower bound for the allowable solution variables.
        If (Allocated(C)) Deallocate( C ); Allocate( C(N) )
        C(:) = 2.0_RP

        !!... Print Option
        IPRINT = 0

        !!... Random Seed
        ISEED1 = this%iseed1
        ISEED2 = this%iseed2

        !!... Temperature
        !!.... Suggested Temperature ::

        Call ptrOptFunc%GetOptValue(N, X, H)

        T = 100.0_RP * dmax1(1.d0/H, 5.d0)

        If (Allocated(VM)) Deallocate( VM ); Allocate( VM(N) )
        VM(:) = 1.0_RP

        If (Allocated(XOPT)) Deallocate( XOPT ); Allocate( XOPT(N) )
        If (Allocated(XP)) Deallocate( XP ); Allocate( XP(N) )
        If (Allocated(NACP)) Deallocate( NACP ); Allocate( NACP(N) )

        If (Allocated(FSTAR)) Deallocate( FSTAR ); Allocate( FSTAR(NEPS) )
        If (Allocated(XP)) Deallocate( XP ); Allocate( XP(NEPS) )

        Call SA(ptrOptFunc, N, X, MAX, RT, EPS, NS, NT,     &
        &       NEPS, MAXEVL, LB, UB, C, IPRINT,            &
        &       ISEED1, ISEED2, T, VM, XOPT, FOPT,          &
        &       NACC, NFCNEV, NOBDS, IER,                   &
        &       FSTAR, XP, NACP                             )

        optVariable(:) = XOPT(:)
        optValue = FOPT

        if ( IER.EQ.1 ) THEN
            isError = .TRUE.
            Call GURU%Warn( &
            &   head  = "Optimize_typSimulatedAnnealing", &
            &   msg   = "Number of function evalaution exeeds the limit.", &
            &   value = NFCNEV )
        elseif ( IER.EQ.2 ) THEN
            isError = .TRUE.
            Call GURU%Warn( &
            &   head  = "Optimize_typSimulatedAnnealing", &
            &   msg   = "Initial values are not in the domain.", &
            &   value = NFCNEV )
        elseif ( IER.EQ.3 ) THEN
            isError = .TRUE.
            Call GURU%Warn( &
            &   head  = "Optimize_typSimulatedAnnealing", &
            &   msg   = "The initial temperature is not positive.", &
            &   value = NFCNEV )
        end if

    End Block

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!