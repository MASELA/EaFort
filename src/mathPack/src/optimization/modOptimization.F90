!! -------------------------------------------------------------------------- !!
!                                   EaFort:GURU                               !!
!! -------------------------------------------------------------------------- !!
!   Optimization Package
!
!   author: Young-Myung Choi
!   date: 2022-05-01
!
!! -------------------------------------------------------------------------- !!
Module modMathPack_Optimization
!! -------------------------------------------------------------------------- !!

    Use modMathPack     !!... Global variables of Math Package

!! -------------------------------------------------------------------------- !!
Implicit None
!! -------------------------------------------------------------------------- !!

Private :: SA, RMARIN, RANMAR, PRT1, PRT2, PRT3, PRT4, PRT6, PRT7, PRT8
Private :: PRT9, PRT10, EXPREP

!!... Optimization Function
#include "optFunc/optFunc.typ"

!!... Simulated Annealing Alogithm Wrapper
#include "simulatedAnnealing/simulatedAnnealing.typ"

!! -------------------------------------------------------------------------- !!
Contains
!! -------------------------------------------------------------------------- !!

!!... Simulated Annealing Alogithm Wrapper
#include "simulatedAnnealing/simulatedAnnealing.inc"
#include "simulatedAnnealing/GoffeAlgorithm/GoffeAlgorithm.inc"

!! -------------------------------------------------------------------------- !!
End Module
!! -------------------------------------------------------------------------- !!