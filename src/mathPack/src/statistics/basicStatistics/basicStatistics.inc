!! -------------------------------------------------------------------------- !!
SUBROUTINE GetDataMoment(data, mean, adev, sigma, var, skewness, kurtosis)
!! -------------------------------------------------------------------------- !!
!   Given an array of data, this routine returns these values:
!       1. Mean                 : mean
!       2. Mean deviation       : adev
!       3. Standard deviation   : sigma
!       4. Variance             : var
!       5. skewness             : skewness
!       6. kurtosis             : kurtosis
!! -------------------------------------------------------------------------- !!
!
!   1. mean     = 1/N * \sum_{j=1}^{N} x_j
!   2. adev     = 1/N * \sum_{j=1}^{N} | x_j - mean |
!   3. sigma    = \sqrt{var}
!   4. var      = 1/(N-1) * [ \sum_{j=1}^{N} x_j^2 - N * mean^2 ]
!   5. skewness = 1/N * \sum_{j=1}^{N} [ (x_j - mean) / sigma ]^3
!   6. kurtosis = 1/N * \sum_{j=1}^{N} [ (x_j - mean) / sigma ]^4 - 3
!
    IMPLICIT NONE
    REAL(RP), DIMENSION(:), INTENT(IN) :: data
    REAL(RP), INTENT(OUT) :: mean, adev, sigma, var, skewness, kurtosis
!! -------------------------------------------------------------------------- !!
    INTEGER  :: n
    REAL(RP) :: ep
    REAL(RP), DIMENSION(size(data)) :: p,s
    n = size(data)

    if (n.LE.1) then
        Call GURU%Error( &
        &   head = "GetDataMoment", &
        &   msg  = "Given 'data' size should be larger than 1.")
    end if

    !... First pass to get the mean.
    mean = sum( data(:) ) / n

    !... Second pass to get the first (absolute), second, third, and
    !    fourth moments of the deviation from the mean.
    s(:) = data(:) - mean
    ep   = sum( s(:) )
    adev = sum( dabs( s(:) ) ) / n
    p(:) = s(:) * s(:)

    var = sum( p(:) )

    p(:) = p(:) * s(:)

    skewness = sum( p(:) )

    p(:)     = p(:)*s(:)
    kurtosis = sum(p(:))

    !... Corrected two-pass formula.
    var = ( var - ep**2 / n) / ( n-1 )

    sigma = dsqrt( dabs(var) )

    if ( dabs(var-0.0).GE.1.D-30 ) then
        skewness = skewness / ( n * sigma**3 )
        kurtosis = kurtosis / ( n * var**2 ) - 3.0_RP
    else
        skewness = 0.0_RP
        kurtosis = 0.0_RP
        Call GURU%Error( &
        &   head = "GetDataMoment", &
        &   msg  = "Skewness and Kurtosis is set to -1 dueto variance is zero. ")
    end if

!! -------------------------------------------------------------------------- !!
END SUBROUTINE GetDataMoment
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
SUBROUTINE GetDataMeanVarStd(data, mean, sigma, var)
!! -------------------------------------------------------------------------- !!
!   Given an array of data, this routine returns these values:
!       1. Mean                 : mean
!       2. Standard deviation   : sigma
!       3. Variance             : var
!! -------------------------------------------------------------------------- !!
!
!   1. mean     = 1/N * \sum_{j=1}^{N} x_j
!   2. sigma    = \sqrt{var}
!   3. var      = 1/(N-1) * [ \sum_{j=1}^{N} x_j^2 - N * mean^2 ]
!
    IMPLICIT NONE
    REAL(RP), DIMENSION(:), INTENT(IN) :: data
    REAL(RP), INTENT(OUT) :: mean, sigma, var
!! -------------------------------------------------------------------------- !!
    INTEGER  :: n
    REAL(RP) :: ep
    REAL(RP), DIMENSION(size(data)) :: p, s
    n = size(data)
    if (n.LE.1) then
        Call GURU%Error( &
        &   head = "GetDataMeanVarStd", &
        &   msg  = "Given 'data' size should be larger than 1.")
    end if
    !... First pass to get the mean.
    mean = sum( data(:) ) / n

    !... Second pass to get the first (absolute), second, third, and
    !    fourth moments of the deviation from the mean.
    s(:) = data(:) - mean
    ep   = sum( s(:) )
    p(:) = s(:) * s(:)
    var = sum( p(:) )

    !... Corrected two-pass formula.
    var = ( var - ep**2 / n) / ( n-1 )
    sigma = dsqrt( dabs(var) )
!! -------------------------------------------------------------------------- !!
END SUBROUTINE GetDataMeanVarStd
!! -------------------------------------------------------------------------- !!
