!! -------------------------------------------------------------------------- !!
Subroutine GetPearsonCoef(x, y, coef)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), dimension(:), intent(in) :: x, y
    Real(RP), intent(out) :: coef
!! -------------------------------------------------------------------------- !!
    Integer :: nX, nY
    Real(RP) :: meanX, meanY, sigmaX, sigmaY
    Real(RP), allocatable :: diffX(:), diffY(:)

    nX = size(x)
    nY = size(y)

    if ( nX.le.0 ) then
        Call GURU%Error( &
        &   head = "GetPearsonCoef", &
        &   msg  = "Given 'x' size is less than 1." )
    end if

    if ( nY.le.0 ) then
        Call GURU%Error( &
        &   head = "GetPearsonCoef", &
        &   msg  = "Given 'y' size is less than 1." )
    end if

    if (nX.ne.nY) then
        Call GURU%Error( &
        &   head = "GetPearsonCoef", &
        &   msg  = "Given 'x' and 'y' size are different." )
    end if

    meanX = sum(x) / nX
    meanY = sum(y) / nX

    If ( Allocated(diffX) ) deallocate(diffX)
    If ( Allocated(diffY) ) deallocate(diffY)

    Allocate( diffX(nX), diffY(nY) )

    diffX(:) = x(:) - meanX
    diffY(:) = y(:) - meanY

    sigmaX = sum( diffX(:) * diffX(:) )
    sigmaY = sum( diffY(:) * diffY(:) )
    coef   = sum ( diffX(:) * diffY(:) ) / dsqrt( sigmaX * sigmaY )

    deallocate( diffX, diffY )

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!