!! -------------------------------------------------------------------------- !!
Subroutine ReadLine(iUnit, line, ier)
!! -------------------------------------------------------------------------- !!
    Implicit none
    Integer, intent(in)                      :: iUnit
    character(len=:),allocatable,intent(out) :: line
    integer,intent(out)                      :: ier
!! -------------------------------------------------------------------------- !!
    integer,parameter                     :: buflen=1024
    character(len=buflen)                 :: buffer
    integer                               :: last
    integer                               :: isize
!! -------------------------------------------------------------------------- !!

    line=''
    ier=0

    ! read characters from line and append to result
    INFINITE: do
        ! read next buffer (an improvement might be to use stream I/O
        ! for files other than stdin so system line limit is not
        ! limiting)
        read(iUnit, iostat=ier,fmt='(a)',advance='no',size=isize) buffer
        ! append what was read to result
        if(isize.gt.0)line=line//buffer(:isize)
        ! if hit EOR reading is complete unless backslash ends the line
        if(is_iostat_eor(ier))then
            last=len(line)
            if(last.ne.0)then
            ! if line ends in backslash it is assumed a continued line
            ! if(line(last:last).eq.'\')then
            !   No escape character in Fortran, so only one backslash here.
            !   Ref: https://fortranwiki.org/fortran/files/character_handling_in_Fortran.html
            if(line(last:last).eq.'\')then
                ! remove backslash
                line=line(:last-1)
                ! continue on and read next line and append to result
                cycle INFINITE
            endif
            endif
            ! hitting end of record is not an error for this routine
            ier=0
            ! end of reading line
            exit INFINITE
        ! end of file or error
        elseif(ier.ne.0)then
        exit INFINITE
        endif
    enddo INFINITE

    line=trim(line)

end subroutine readline