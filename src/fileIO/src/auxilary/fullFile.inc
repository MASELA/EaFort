!! -------------------------------------------------------------------------- !!
Subroutine FullFile(dirPath, fileName, fullPath)
!! -------------------------------------------------------------------------- !!
    Character(len=*), intent(in) :: dirPath
    Character(len=*), intent(in) :: fileName
    Type(typString), intent(out) :: fullPath
!! -------------------------------------------------------------------------- !!
    Character(len=:), Allocatable :: dirPath_, fileName_, fullPath_
    Integer :: n

    dirPath_  = trim(adjustl(dirPath))
    fileName_ = trim(adjustl(fileName))

    n = len_trim(dirPath)
    if ( n.le.0 ) then
        fullPath_ = fileName_
    else
        if ( IsStringEqual(dirPath_(n:n), CHAR_DIR_SEP) ) then
            fullPath_ = dirPath_//fileName_
        else
            fullPath_ = dirPath_//CHAR_DIR_SEP//fileName_
        end if
    end if

    fullPath = fullPath_

    If (Allocated(dirPath_)) Deallocate(dirPath_)
    If (Allocated(fileName_)) Deallocate(fileName_)
    If (Allocated(fullPath_)) Deallocate(fullPath_)

End Subroutine
!! -------------------------------------------------------------------------- !!
