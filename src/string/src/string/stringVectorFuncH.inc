!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray1(from, to, nCopy1)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:), to(:)
    Integer, Optional :: nCopy1
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1
    Integer :: iStr1
    Integer :: nCopy1_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray1", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        Allocate( to(nStr1) )

        do iStr1 = 1, nCopy1_
            to(iStr1) = from(iStr1)
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray1(from, to)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray2(from, to, nCopy1, nCopy2)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:, :), to(:, :)
    Integer, Optional :: nCopy1, nCopy2
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1, nStr2
    Integer :: iStr1, iStr2
    Integer :: nCopy1_, nCopy2_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )
        nStr2 = size( from, 2 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray2", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        nCopy2_ = nStr2
        if ( present(nCopy2) ) then
            if ( nCopy2.gt.nStr2 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray2", &
                &   msg   = "Given 'nCopy2' is larger than the size of 'from'. ", &
                &   value = "nCopy2: "//Int2Char(nCopy2,"(i5)")//" size(from, 2): "//Int2Char(nStr2,"(i5)") )
            else
                nCopy2_ = nCopy2
            end if
        end if

        Allocate( to(nStr1, nStr2) )

        do iStr2 = 1, nCopy2_
        do iStr1 = 1, nCopy1_
            to(iStr1, iStr2) = from(iStr1, iStr2)
        end do
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray2(from, to)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray3(from, to, nCopy1, nCopy2, nCopy3)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:, :, :), to(:, :, :)
    Integer, Optional :: nCopy1, nCopy2, nCopy3
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1, nStr2, nStr3
    Integer :: iStr1, iStr2, iStr3
    Integer :: nCopy1_, nCopy2_, nCopy3_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )
        nStr2 = size( from, 2 )
        nStr3 = size( from, 3 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray3", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        nCopy2_ = nStr2
        if ( present(nCopy2) ) then
            if ( nCopy2.gt.nStr2 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray3", &
                &   msg   = "Given 'nCopy2' is larger than the size of 'from'. ", &
                &   value = "nCopy2: "//Int2Char(nCopy2,"(i5)")//" size(from, 2): "//Int2Char(nStr2,"(i5)") )
            else
                nCopy2_ = nCopy2
            end if
        end if

        nCopy3_ = nStr3
        if ( present(nCopy3) ) then
            if ( nCopy3.gt.nStr3 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray3", &
                &   msg   = "Given 'nCopy3' is larger than the size of 'from'. ", &
                &   value = "nCopy3: "//Int2Char(nCopy3,"(i5)")//" size(from, 3): "//Int2Char(nStr3,"(i5)") )
            else
                nCopy3_ = nCopy3
            end if
        end if

        Allocate( to(nStr1, nStr2, nStr3) )

        do iStr3 = 1, nCopy3_
        do iStr2 = 1, nCopy2_
        do iStr1 = 1, nCopy1_
            to(iStr1, iStr2, iStr3) = from(iStr1, iStr2, iStr3)
        end do
        end do
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray2(from, to, nCopy1)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray4(from, to, nCopy1, nCopy2, nCopy3, nCopy4)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:, :, :, :), to(:, :, :, :)
    Integer, Optional :: nCopy1, nCopy2, nCopy3, nCopy4
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1, nStr2, nStr3, nStr4
    Integer :: iStr1, iStr2, iStr3, iStr4
    Integer :: nCopy1_, nCopy2_, nCopy3_, nCopy4_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )
        nStr2 = size( from, 2 )
        nStr3 = size( from, 3 )
        nStr4 = size( from, 4 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray4", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        nCopy2_ = nStr2
        if ( present(nCopy2) ) then
            if ( nCopy2.gt.nStr2 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray4", &
                &   msg   = "Given 'nCopy2' is larger than the size of 'from'. ", &
                &   value = "nCopy2: "//Int2Char(nCopy2,"(i5)")//" size(from, 2): "//Int2Char(nStr2,"(i5)") )
            else
                nCopy2_ = nCopy2
            end if
        end if

        nCopy3_ = nStr3
        if ( present(nCopy3) ) then
            if ( nCopy3.gt.nStr3 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray4", &
                &   msg   = "Given 'nCopy3' is larger than the size of 'from'. ", &
                &   value = "nCopy3: "//Int2Char(nCopy3,"(i5)")//" size(from, 3): "//Int2Char(nStr3,"(i5)") )
            else
                nCopy3_ = nCopy3
            end if
        end if

        nCopy4_ = nStr4
        if ( present(nCopy4) ) then
            if ( nCopy4.gt.nStr4 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray4", &
                &   msg   = "Given 'nCopy4' is larger than the size of 'from'. ", &
                &   value = "nCopy4: "//Int2Char(nCopy4,"(i5)")//" size(from, 4): "//Int2Char(nStr4,"(i5)") )
            else
                nCopy4_ = nCopy4
            end if
        end if

        Allocate( to(nStr1, nStr2, nStr3, nStr4) )

        do iStr4 = 1, nCopy4_
        do iStr3 = 1, nCopy3_
        do iStr2 = 1, nCopy2_
        do iStr1 = 1, nCopy1_
            to(iStr1, iStr2, iStr3, iStr4) = from(iStr1, iStr2, iStr3, iStr4)
        end do
        end do
        end do
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray4(from, to)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray5(from, to, nCopy1, nCopy2, nCopy3, nCopy4, nCopy5)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:, :, :, :, :), to(:, :, :, :, :)
    Integer, Optional :: nCopy1, nCopy2, nCopy3, nCopy4, nCopy5
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1, nStr2, nStr3, nStr4, nStr5
    Integer :: iStr1, iStr2, iStr3, iStr4, iStr5
    Integer :: nCopy1_, nCopy2_, nCopy3_, nCopy4_, nCopy5_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )
        nStr2 = size( from, 2 )
        nStr3 = size( from, 3 )
        nStr4 = size( from, 4 )
        nStr5 = size( from, 5 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray5", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        nCopy2_ = nStr2
        if ( present(nCopy2) ) then
            if ( nCopy2.gt.nStr2 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray5", &
                &   msg   = "Given 'nCopy2' is larger than the size of 'from'. ", &
                &   value = "nCopy2: "//Int2Char(nCopy2,"(i5)")//" size(from, 2): "//Int2Char(nStr2,"(i5)") )
            else
                nCopy2_ = nCopy2
            end if
        end if

        nCopy3_ = nStr3
        if ( present(nCopy3) ) then
            if ( nCopy3.gt.nStr3 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray5", &
                &   msg   = "Given 'nCopy3' is larger than the size of 'from'. ", &
                &   value = "nCopy3: "//Int2Char(nCopy3,"(i5)")//" size(from, 3): "//Int2Char(nStr3,"(i5)") )
            else
                nCopy3_ = nCopy3
            end if
        end if

        nCopy4_ = nStr4
        if ( present(nCopy4) ) then
            if ( nCopy4.gt.nStr4 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray5", &
                &   msg   = "Given 'nCopy4' is larger than the size of 'from'. ", &
                &   value = "nCopy4: "//Int2Char(nCopy4,"(i5)")//" size(from, 4): "//Int2Char(nStr4,"(i5)") )
            else
                nCopy4_ = nCopy4
            end if
        end if

        nCopy5_ = nStr5
        if ( present(nCopy5) ) then
            if ( nCopy5.gt.nStr5 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray5", &
                &   msg   = "Given 'nCopy5' is larger than the size of 'from'. ", &
                &   value = "nCopy5: "//Int2Char(nCopy5,"(i5)")//" size(from, 5): "//Int2Char(nStr5,"(i5)") )
            else
                nCopy5_ = nCopy5
            end if
        end if

        Allocate( to(nStr1, nStr2, nStr3, nStr4, nStr5) )

        do iStr5 = 1, nCopy5_
        do iStr4 = 1, nCopy4_
        do iStr3 = 1, nCopy3_
        do iStr2 = 1, nCopy2_
        do iStr1 = 1, nCopy1_
            to(iStr1, iStr2, iStr3, iStr4, iStr5) = from(iStr1, iStr2, iStr3, iStr4, iStr5)
        end do
        end do
        end do
        end do
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray5(from, to)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CopyStringArray6(from, to, nCopy1, nCopy2, nCopy3, nCopy4, nCopy5, nCopy6)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Type(typString), Allocatable :: from(:, :, :, :, :, :), to(:, :, :, :, :, :)
    Integer, Optional :: nCopy1, nCopy2, nCopy3, nCopy4, nCopy5, nCopy6
!! -------------------------------------------------------------------------- !!
    Integer :: nStr1, nStr2, nStr3, nStr4, nStr5, nStr6
    Integer :: iStr1, iStr2, iStr3, iStr4, iStr5, iStr6
    Integer :: nCopy1_, nCopy2_, nCopy3_, nCopy4_, nCopy5_, nCopy6_

    !!... Deallocate
    Call DeallocateStringArray( to )

    !!... Copy
    if ( Allocated(from) ) then

        nStr1 = size( from, 1 )
        nStr2 = size( from, 2 )
        nStr3 = size( from, 3 )
        nStr4 = size( from, 4 )
        nStr5 = size( from, 5 )
        nStr6 = size( from, 6 )

        nCopy1_ = nStr1
        if ( present(nCopy1) ) then
            if ( nCopy1.gt.nStr1 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy1' is larger than the size of 'from'. ", &
                &   value = "nCopy1: "//Int2Char(nCopy1,"(i5)")//" size(from, 1): "//Int2Char(nStr1,"(i5)") )
            else
                nCopy1_ = nCopy1
            end if
        end if

        nCopy2_ = nStr2
        if ( present(nCopy2) ) then
            if ( nCopy2.gt.nStr2 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy2' is larger than the size of 'from'. ", &
                &   value = "nCopy2: "//Int2Char(nCopy2,"(i5)")//" size(from, 2): "//Int2Char(nStr2,"(i5)") )
            else
                nCopy2_ = nCopy2
            end if
        end if

        nCopy3_ = nStr3
        if ( present(nCopy3) ) then
            if ( nCopy3.gt.nStr3 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy3' is larger than the size of 'from'. ", &
                &   value = "nCopy3: "//Int2Char(nCopy3,"(i5)")//" size(from, 3): "//Int2Char(nStr3,"(i5)") )
            else
                nCopy3_ = nCopy3
            end if
        end if

        nCopy4_ = nStr4
        if ( present(nCopy4) ) then
            if ( nCopy4.gt.nStr4 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy4' is larger than the size of 'from'. ", &
                &   value = "nCopy4: "//Int2Char(nCopy4,"(i5)")//" size(from, 4): "//Int2Char(nStr4,"(i5)") )
            else
                nCopy4_ = nCopy4
            end if
        end if

        nCopy5_ = nStr5
        if ( present(nCopy5) ) then
            if ( nCopy5.gt.nStr5 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy5' is larger than the size of 'from'. ", &
                &   value = "nCopy5: "//Int2Char(nCopy5,"(i5)")//" size(from, 5): "//Int2Char(nStr5,"(i5)") )
            else
                nCopy5_ = nCopy5
            end if
        end if

        nCopy6_ = nStr6
        if ( present(nCopy6) ) then
            if ( nCopy6.gt.nStr6 ) then
                Call GURU%Error( &
                &   head  = "CopyStringArray6", &
                &   msg   = "Given 'nCopy6' is larger than the size of 'from'. ", &
                &   value = "nCopy6: "//Int2Char(nCopy6,"(i5)")//" size(from, 6): "//Int2Char(nStr6,"(i5)") )
            else
                nCopy6_ = nCopy6
            end if
        end if

        Allocate( to(nStr1, nStr2, nStr3, nStr4, nStr5, nStr6) )

        do iStr6 = 1, nCopy6_
        do iStr5 = 1, nCopy5_
        do iStr4 = 1, nCopy4_
        do iStr3 = 1, nCopy3_
        do iStr2 = 1, nCopy2_
        do iStr1 = 1, nCopy1_
            to(iStr1, iStr2, iStr3, iStr4, iStr5, iStr6) = from(iStr1, iStr2, iStr3, iStr4, iStr5, iStr6)
        end do
        end do
        end do
        end do
        end do
        end do

    else
        Call GURU%Debug( &
        &   head = "CopyStringArray5(from, to)", &
        &   msg  = "Size of string array 'from' is zero. No copy process is conducted.")
    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray1( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1
    Integer :: nStr1
    if ( Allocated(strArr) ) then
        nStr1 = size( strArr )
        do iStr1 = 1, nStr1
            Call strArr(iStr1)%Destroy()
        end do
        Deallocate( strArr )
    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray2( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:, :)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1, iStr2
    Integer :: nStr1, nStr2

    if ( Allocated(strArr) ) then

        nStr1 = size( strArr, 1 )
        nStr2 = size( strArr, 2 )

        do iStr2 = 1, nStr2
            do iStr1 = 1, nStr1
                Call strArr(iStr1, iStr2)%Destroy()
            end do
        end do

        Deallocate( strArr )

    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray3( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:, :, :)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1, iStr2, iStr3
    Integer :: nStr1, nStr2, nStr3

    if ( Allocated(strArr) ) then

        nStr1 = size( strArr, 1 )
        nStr2 = size( strArr, 2 )
        nStr3 = size( strArr, 3 )

        do iStr3 = 1, nStr3
        do iStr2 = 1, nStr2
        do iStr1 = 1, nStr1
            Call strArr(iStr1, iStr2, iStr3)%Destroy()
        end do
        end do
        end do

        Deallocate( strArr )

    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray4( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:, :, :, :)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1, iStr2, iStr3, iStr4
    Integer :: nStr1, nStr2, nStr3, nStr4

    if ( Allocated(strArr) ) then

        nStr1 = size( strArr, 1 )
        nStr2 = size( strArr, 2 )
        nStr3 = size( strArr, 3 )
        nStr4 = size( strArr, 4 )

        do iStr4 = 1, nStr4
        do iStr3 = 1, nStr3
        do iStr2 = 1, nStr2
        do iStr1 = 1, nStr1
            Call strArr(iStr1, iStr2, iStr3, iStr4)%Destroy()
        end do
        end do
        end do
        end do

        Deallocate( strArr )

    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray5( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:, :, :, :, :)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1, iStr2, iStr3, iStr4, iStr5
    Integer :: nStr1, nStr2, nStr3, nStr4, nStr5

    if ( Allocated(strArr) ) then

        nStr1 = size( strArr, 1 )
        nStr2 = size( strArr, 2 )
        nStr3 = size( strArr, 3 )
        nStr4 = size( strArr, 4 )
        nStr5 = size( strArr, 5 )

        do iStr5 = 1, nStr5
        do iStr4 = 1, nStr4
        do iStr3 = 1, nStr3
        do iStr2 = 1, nStr2
        do iStr1 = 1, nStr1
            Call strArr(iStr1, iStr2, iStr3, iStr4, iStr5)%Destroy()
        end do
        end do
        end do
        end do
        end do

        Deallocate( strArr )

    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine DeallocateStringArray6( strArr )
    Implicit None
    Type(typString), Allocatable :: strArr(:, :, :, :, :, :)
!! -------------------------------------------------------------------------- !!
    Integer :: iStr1, iStr2, iStr3, iStr4, iStr5, iStr6
    Integer :: nStr1, nStr2, nStr3, nStr4, nStr5, nStr6

    if ( Allocated(strArr) ) then

        nStr1 = size( strArr, 1 )
        nStr2 = size( strArr, 2 )
        nStr3 = size( strArr, 3 )
        nStr4 = size( strArr, 4 )
        nStr5 = size( strArr, 5 )
        nStr6 = size( strArr, 6 )

        do iStr6 = 1, nStr6
        do iStr5 = 1, nStr5
        do iStr4 = 1, nStr4
        do iStr3 = 1, nStr3
        do iStr2 = 1, nStr2
        do iStr1 = 1, nStr1
            Call strArr(iStr1, iStr2, iStr3, iStr4, iStr5, iStr6)%Destroy()
        end do
        end do
        end do
        end do
        end do
        end do

        Deallocate( strArr )

    end if
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!