!! -------------------------------------------------------------------------- !!
Subroutine Initialize_typFuncODE_FreeDecay(this, mass, damp, rest)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Class(typFuncODE_FreeDecay), intent(inout) :: this
    Real(RP), intent(in) :: mass, damp, rest
!! -------------------------------------------------------------------------- !!
    this%mass = mass
    this%damp = damp
    this%rest = rest
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine GetDyDxReal_typFuncODE_FreeDecay(this, x, y, dydx)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Class(typFuncODE_FreeDecay), intent(inout) :: this
    Real(RP), intent(in)                :: x
    Real(RP), dimension(:), intent(in)  :: y
    Real(RP), dimension(:), intent(out) :: dydx
!! -------------------------------------------------------------------------- !!
    !!... Set Variables
    dydx(1) = y(2)
    dydx(2) = ( -this%damp * y(2) - this%rest * y(1) ) / this%mass
!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!