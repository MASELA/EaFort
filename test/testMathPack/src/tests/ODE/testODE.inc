!! -------------------------------------------------------------------------- !!
Subroutine testODE()
!! -------------------------------------------------------------------------- !!
    Implicit None
!! -------------------------------------------------------------------------- !!
    Real(RP) :: t, dt
    Type(typFuncODE_FreeDecay), Target :: funcODE_FreeDecay
    Class(typFuncTypeODE), Pointer :: ptrFuncODE
    Integer :: nTime, iTime
    Real(RP) :: mass, damp, rest, criticalDamp, zeta
    Real(RP) :: y0, dy0dt

    t  = 0.0_RP
    dt = 0.1_RP

    nTime = 1000

    mass = 1.0_RP
    rest = 1.0_RP

    zeta = 1.0_RP

    criticalDamp = 2.0_RP * dsqrt( mass * rest )

    damp = zeta * criticalDamp

    ! !!.... Set Initial Conditions
    ! y0    = 1.0_RP
    ! dy0dt = 0.5_RP

    y0    = 0.0_RP
    dy0dt = 0.5_RP

    Call funcODE_FreeDecay%Initialize( mass, damp, rest )
    ptrFuncODE => funcODE_FreeDecay


    ! Real(RP), Allocatable :: y(:), dydx(:), yout(:), ysCal(:)
    ! Real(RP), Allocatable :: yAdaptRK5(:), dydxAdaptRK5(:), youtAdaptRK5(:)
    ! Real(RP), Allocatable :: ySave(:, :)
    ! Integer :: iUnitRK4, iUnitRK5, iUnitGBS
    ! Real(RP) :: mass, damp, rest, ySol, dydtSol, y0, dy0dt
    ! Real(RP) :: hTry, errorBound, hDid, hNext

    ! Type(typSolveODE_BulirschStoer) :: SolveODE_BS

    ! !!... Runge-Kutta 4th
    blkRK4: Block
        Integer :: iUnit
        Real(RP), Allocatable :: y(:), dydx(:), yout(:)
        Real(RP), Allocatable :: ySave(:, :)
        Real(RP), Allocatable :: ySol(:), dydtSol(:)
        Real(RP) :: coefY, coefdYdx

        Allocate( y(2), dydx(2), yout(2) )
        Allocate( ySave(3, nTime) )
        Allocate( ySol(nTime), dydtSol(nTime) )

        y(1) = y0
        y(2) = dy0dt

        t  = 0.0_RP

        ySave(1, 1)   = t
        ySave(2:3, 1) = y(:)

        do iTime = 2, nTime
            Call ptrFuncODE%GetDyDx( t, y, dydx )
            Call SolveODE_RK4( y, dydx, t, dt, yout, ptrFuncODE )

            !!... Save
            ySave(1, iTime)   = t
            ySave(2:3, iTime) = y(:)

            !... Update
            t    = t + dt
            y(:) = yout(:)
        end do

        Open(newunit = iUnit, file = "freeDecay_RK4.txt", status = "replace" )

        do iTime = 1, nTime
            t = ySave(1, iTime)
            Call GetFreeDecayTimeSeries(mass, damp, rest, y0, dy0dt, t, ySol(iTime), dydtSol(iTime) )
            write(iUnit, 1001) ySave(:, iTime), ySol(iTime), dydtSol(iTime)
        end do

        !!... Get Correlation Coefficients
        Call GetPearsonCoef( ySave(2, :), ySol(:), coefY )
        Call GetPearsonCoef( ySave(3, :), dydtSol(:), coefdYdx )

        Call GURU%Write( &
        &   "RK4: Correlation Coef: "//Real2Char(coefY)//" , "//Real2Char(coefdYdx) )

        close(iUnit)

    End Block blkRK4

    !!... Adaptive RK 5
    blkAdaptRK5: Block
        Integer :: iUnit
        Real(RP), Allocatable :: y(:), dydx(:), yout(:)
        Real(RP), Allocatable :: ySave(:, :)
        Real(RP), Allocatable :: ySol(:), dydtSol(:), ysCal(:)
        Real(RP) :: coefY, coefdYdx
        Real(RP) :: hTry, hdid, hNext, errorBound

        Allocate( y(2), dydx(2), yout(2), ysCal(2) )
        Allocate( ySave(3, nTime) )
        Allocate( ySol(nTime), dydtSol(nTime) )

        y(1) = y0
        y(2) = dy0dt

        ysCal(:) = 1.0_RP

        t    = 0.0_RP
        hTry = dt

        errorBound = 1.D-4

        ySave(1, 1)   = t
        ySave(2:3, 1) = y(:)

        do iTime = 2, nTime
            Call ptrFuncODE%GetDyDx( t, y, dydx )

            Call SolveODE_AdaptiveRK5_Real( &
                y, dydx, t, hTry, errorBound, ysCal, hdid, hNext, ptrFuncODE)

            hTry = hNext

            !!... Save
            ySave(1, iTime)   = t
            ySave(2:3, iTime) = y(:)

        end do

        Open(newunit = iUnit, file = "freeDecay_AdaptRK5.txt", status = "replace" )

        do iTime = 1, nTime
            t = ySave(1, iTime)
            Call GetFreeDecayTimeSeries(mass, damp, rest, y0, dy0dt, t, ySol(iTime), dydtSol(iTime) )
            write(iUnit, 1001) ySave(:, iTime), ySol(iTime), dydtSol(iTime)
        end do

        !!... Get Correlation Coefficients
        Call GetPearsonCoef( ySave(2, :), ySol(:), coefY )
        Call GetPearsonCoef( ySave(3, :), dydtSol(:), coefdYdx )

        Call GURU%Write( &
        &   "Adapt RK5: Correlation Coef: "//Real2Char(coefY)//" , "//Real2Char(coefdYdx) )

        close(iUnit)

    End Block blkAdaptRK5

    !!... Bulisch-Stoer Method
    blkGBS: Block
        Integer :: iUnit
        Real(RP), Allocatable :: y(:), dydx(:), yout(:)
        Real(RP), Allocatable :: ySave(:, :)
        Real(RP), Allocatable :: ySol(:), dydtSol(:), ysCal(:)
        Real(RP) :: coefY, coefdYdx
        Real(RP) :: hTry, hdid, hNext, errorBound
        Type(typSolveODE_BulirschStoer) :: ODESolver_GBS

        Allocate( y(2), dydx(2), yout(2), ysCal(2) )
        Allocate( ySave(3, nTime) )
        Allocate( ySol(nTime), dydtSol(nTime) )

        y(1) = y0
        y(2) = dy0dt

        ysCal(:) = 1.0_RP

        t    = 0.0_RP
        hTry = dt

        ySave(1, 1)   = t
        ySave(2:3, 1) = y(:)

        errorBound = 1.D-4
        do iTime = 2, nTime
            Call ptrFuncODE%GetDyDx( t, y, dydx )

            Call ODESolver_GBS%SolveODE( &
            &   y, dydx, t, hTry, errorBound, ysCal, hDid, hNext, ptrFuncODE )

            hTry = hNext

            !!... Save
            ySave(1, iTime)   = t
            ySave(2:3, iTime) = y(:)

        end do

        Open(newunit = iUnit, file = "freeDecay_GBS.txt", status = "replace" )

        do iTime = 1, nTime
            t = ySave(1, iTime)
            Call GetFreeDecayTimeSeries(mass, damp, rest, y0, dy0dt, t, ySol(iTime), dydtSol(iTime) )
            write(iUnit, 1001) ySave(:, iTime), ySol(iTime), dydtSol(iTime)
        end do

        !!... Get Correlation Coefficients
        Call GetPearsonCoef( ySave(2, :), ySol(:), coefY )
        Call GetPearsonCoef( ySave(3, :), dydtSol(:), coefdYdx )

        Call GURU%Write( &
        &   "GBS: Correlation Coef: "//Real2Char(coefY)//" , "//Real2Char(coefdYdx) )

        close(iUnit)

    End Block blkGBS

    !!... Rosenbrock fifth order Method
    blkRosenbrock5: Block
        Integer :: iUnit
        Real(RP), Allocatable :: y(:), dydx(:), yout(:)
        Real(RP), Allocatable :: ySave(:, :)
        Real(RP), Allocatable :: ySol(:), dydtSol(:), ysCal(:)
        Real(RP) :: coefY, coefdYdx
        Real(RP) :: hTry, hdid, hNext, errorBound

        Allocate( y(2), dydx(2), yout(2), ysCal(2) )
        Allocate( ySave(3, nTime) )
        Allocate( ySol(nTime), dydtSol(nTime) )

        y(1) = y0
        y(2) = dy0dt

        ysCal(:) = 1.0_RP

        t    = 0.0_RP
        hTry = dt

        ySave(1, 1)   = t
        ySave(2:3, 1) = y(:)

        errorBound = 1.D-4
        do iTime = 2, nTime
            Call ptrFuncODE%GetDyDx( t, y, dydx )

            Call SolveODE_Rosenbrock5( &
            &   y, dydx, t, hTry, errorBound, ysCal, hDid, hNext, ptrFuncODE, Jacobn_FreeDecay )

            hTry = hNext

            !!... Save
            ySave(1, iTime)   = t
            ySave(2:3, iTime) = y(:)

        end do

        Open(newunit = iUnit, file = "freeDecay_Rosenbrock5.txt", status = "replace" )

        do iTime = 1, nTime
            t = ySave(1, iTime)
            Call GetFreeDecayTimeSeries(mass, damp, rest, y0, dy0dt, t, ySol(iTime), dydtSol(iTime) )
            write(iUnit, 1001) ySave(:, iTime), ySol(iTime), dydtSol(iTime)
        end do

        !!... Get Correlation Coefficients
        Call GetPearsonCoef( ySave(2, :), ySol(:), coefY )
        Call GetPearsonCoef( ySave(3, :), dydtSol(:), coefdYdx )

        Call GURU%Write( &
        &   "Rosenbrock5: Correlation Coef: "//Real2Char(coefY)//" , "//Real2Char(coefdYdx) )

        close(iUnit)

    End Block blkRosenbrock5



    1001 format(9999(1pes16.6E3) )
    ! 1001 format(9999(1pe15.6) )

Contains

    SUBROUTINE Jacobn_FreeDecay(x, y, dfdx, dfdy)
        IMPLICIT NONE
        REAL(RP), INTENT(IN) :: x
        REAL(RP), DIMENSION(:), INTENT(IN)    :: y
        REAL(RP), DIMENSION(:), INTENT(OUT)   :: dfdx
        REAL(RP), DIMENSION(:,:), INTENT(OUT) :: dfdy
        dfdx(:)   =    0.0_RP
        dfdy(1,1) =    0.0_RP
        dfdy(1,2) =    1.0_RP
        dfdy(2,1) =  - rest / mass
        dfdy(2,2) =  - damp / mass
    END SUBROUTINE Jacobn_FreeDecay

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!
