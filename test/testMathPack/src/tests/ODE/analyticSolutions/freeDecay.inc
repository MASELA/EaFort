!! -------------------------------------------------------------------------- !!
Subroutine GetFreeDecayTimeSeries(m, b, k, x0, dx0dt, t, x, dxdt)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP), intent(in)  :: m, b, k, t, x0, dx0dt
    Real(RP), intent(out) :: x, dxdt
!! -------------------------------------------------------------------------- !!
    Real(RP) :: gamma, omega0, omegan
    Real(RP) :: sqrtOneMGamma
    Real(RP) :: expFac, expFac2, timeFac, timeFac2, fac

    omega0 = dsqrt(k / m)
    gamma  = b / 2.0_RP / m / omega0

    sqrtOneMGamma = dsqrt( dabs(1.0_RP - gamma*gamma) )

    omegan = omega0 * sqrtOneMGamma

    if (gamma < 1.0_RP - 1.D-6) then

        expFac  = dexp(-gamma*omega0 * t)
        expFac2 = -gamma * omega0 * expFac

        fac =  dx0dt / omegan + gamma / sqrtOneMGamma * x0

        timeFac  =  x0 * dcos(omegan * t)          + fac * dsin(omegan * t)
        timeFac2 = -x0 * dsin(omegan * t) * omegan + fac * dcos(omegan * t) * omegan

        x    = expFac  * timeFac
        dxdt = expFac2 * timeFac + expFac * timeFac2

    else if (gamma > 1.0_RP + 1.D-6) then

        expFac  = dexp(-gamma*omega0 * t)
        expFac2 = -gamma * omega0 * expFac

        fac =  dx0dt / omegan + gamma / sqrtOneMGamma * x0

        timeFac  =  x0 * dcosh(omegan * t)          + fac * dsinh(omegan * t)
        timeFac2 =  x0 * dsinh(omegan * t) * omegan + fac * dcosh(omegan * t) * omegan

        x    = expFac  * timeFac
        dxdt = expFac2 * timeFac + expFac * timeFac2

    else

        expFac  =   dexp(-omega0 * t)
        expFac2 = - omega0 * expFac

        fac =  dx0dt + omega0 * x0

        timeFac  = x0 + fac * t
        timeFac2 = fac

        x    = expFac  * timeFac
        dxdt = expFac2 * timeFac + expFac * timeFac2

    end if

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!